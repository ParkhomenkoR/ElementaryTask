package main

import (
	"fmt"
	"math"
	"os"
)

/*Вывести через запятую ряд длиной n, состоящий из натуральных чисел, квадрат которых не меньше заданного m.

Входные параметры: длина и значение минимального квадрата
Выход: строка с рядом чисел
*/

func main() {
	for true {
		var n int
		fmt.Println("введите длину")
		fmt.Fscan(os.Stdin, &n)
		var m int
		fmt.Println("квадрат")
		fmt.Fscan(os.Stdin, &m)
		if n != 0 && m != 0 && n > 0 && m > 0 {

			var min float64 = math.Sqrt(float64(m))
			var trueMin int = int(min)

			for i := trueMin + 1; i < n+trueMin; i++ {
				fmt.Print(i, ",")

			}

			fmt.Println(trueMin + n)
			break
		}
		fmt.Println("n и m должны быть числами больше 0")
	}
}

// go run .\main\task6.go
