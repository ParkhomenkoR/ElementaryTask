package main

import (
	"fmt"
	"math"
	"os"
	"sort"
)

type Triangle struct {
	Vertices string
	A        float64
	B        float64
	C        float64
	Area     float64
}

func (t *Triangle) getArea() {
	p := 0.5 * (t.A + t.B + t.C)
	t.Area = math.Sqrt(p * (p - t.A) * (p - t.B) * (p - t.C))

}

func getTriangles() []Triangle {
	triangleSlice := make([]Triangle, 0)
	var t Triangle
	for i := 0; i < 3; i++ {
		fmt.Fscan(os.Stdin, &t.Vertices, &t.A, &t.B, &t.C)

		if t.A <= 0 || t.B <= 0 || t.C <= 0 {
			fmt.Println("wrong one of  triangles")
			break
		}
		if t.A+t.B < t.C || t.A+t.C < t.B || t.B+t.C < t.A {
			fmt.Println("wrong one of  triangles")
			break
		}
		t.getArea()
		triangleSlice = append(triangleSlice, t)

	}
	return triangleSlice
}
func main() {
	trianglesArray := getTriangles()

	sort.Slice(trianglesArray, func(i, j int) bool {
		return trianglesArray[i].Area > trianglesArray[j].Area
	})
	printResultTriangles(trianglesArray)
}

func printResultTriangles(trianglesArray []Triangle) {
	for _, el := range trianglesArray {
		fmt.Printf(el.Vertices)
		fmt.Print(" ")
	}
}
