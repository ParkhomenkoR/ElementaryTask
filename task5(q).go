package main

import (
	"fmt"
	"os"
)

var (
	min           int
	max           int
	firstNumeral  int
	secondNumeral int
	thirdNumeral  int
	fourthNumeral int
	fifthNumeral  int
	sixNumeral    int
	simpleSum1    int
	simpleSum2    int
	hardSum1      int
	hardSum2      int
	k             int = 0
	m             int = 0
)

func main() {
	checkAndGo()
}

func calcul(min int, max int) int {
	for i := min; i <= max; i++ {
		parsing(i)
		simpleSum1 = firstNumeral + secondNumeral + thirdNumeral
		simpleSum2 = fourthNumeral + fifthNumeral + sixNumeral
		if simpleSum1 == simpleSum2 {
			k++
		}

	}
	return k
}
func calculOddEven(min int, max int) int {

	for i := min; i <= max; i++ {
		evenSum, oddSum := 0, 0
		parsing(i)

		if firstNumeral%2 == 0 {
			evenSum += firstNumeral
		} else {
			oddSum += firstNumeral
		}
		if secondNumeral%2 == 0 {
			evenSum += secondNumeral
		} else {
			oddSum += secondNumeral
		}

		if thirdNumeral%2 == 0 {
			evenSum += thirdNumeral
		} else {
			oddSum += thirdNumeral
		}

		if fourthNumeral%2 == 0 {
			evenSum += fourthNumeral
		} else {
			oddSum += fourthNumeral
		}

		if fifthNumeral%2 == 0 {
			evenSum += fifthNumeral
		} else {
			oddSum += fifthNumeral
		}

		if sixNumeral%2 == 0 {
			evenSum += sixNumeral
		} else {
			oddSum += sixNumeral
		}

		if oddSum == evenSum {
			m++
		}
	}

	return m
}

func parsing(i int) {
	firstNumeral = i / 100000
	secondNumeral = (i % 100000) / 10000
	thirdNumeral = (i % 10000) / 1000
	fourthNumeral = (i % 1000) / 100
	fifthNumeral = (i % 100) / 10
	sixNumeral = i % 10
}

func checkAndGo() {
	fmt.Println("Введитие мин знач")
	fmt.Fscan(os.Stdin, &min)

	fmt.Println("Введитие мах знач")
	fmt.Fscan(os.Stdin, &max)
	if min > 0 && max > 0 && max > min && min < 999999 && max < 999999 && min > 100000 {
		calcul(min, max)
		calculOddEven(min, max)
		fmt.Println("Простой способ: ", k)
		fmt.Println("Сложный способ: ", m)

		if k > m {
			fmt.Println("Выиграл простой способ")
		}
		if k < m {
			fmt.Println("Выиграл сложный способ")
		}
		if k == m {
			fmt.Println("ничья")
		}
	} else {
		fmt.Println()
		fmt.Println("Будьте добры ввести 6-значное число>0")
		main()
	}

}
