package main

import (
	"fmt"
	"os"
)

/*7.	Ряд Фибоначчи для диапазона
Вывести все числа Фибоначчи, которые удовлетворяют переданному в функцию ограничению: находятся в указанном диапазоне, либо имеют указанную длину.

Входные параметры: объект context с полями min и max, либо с полем length
Выход: массив чисел
*/

func main() {
	var choice int
	fmt.Println("введите 1, если хотите увидеть ряд Ф определенной длинны, введите 2, если хотите задать диапазон")
	fmt.Fscan(os.Stdin, &choice)

	if choice == 1 {
		var len int
		fmt.Println("введите длинну ")
		fmt.Fscan(os.Stdin, &len)
		length(len)
	}

	if choice == 2 {
		var min int
		fmt.Println("минимальное значение ")
		fmt.Fscan(os.Stdin, &min)

		var max int
		fmt.Println("максимальное значение ")
		fmt.Fscan(os.Stdin, &max)
		diapazon(min, max)
	}
}

func f(index int) int {
	if index <= 0 {
		return 0
	} else if index == 1 {
		return 1
	} else if index == 2 {
		return 1
	} else {
		return f(index-1) + f(index-2)
	}
}

func diapazon(min, max int) {
	var len int = 46
	for i := 1; i <= len; i++ {
		if f(i) <= max && f(i) >= min {
			fmt.Print(f(i), "  ")
		}
		if f(i) > max {
			break
		}
	}
}

func length(len int) {
	for i := 1; i <= len; i++ {
		{
			fmt.Print(f(i), "  ")
		}
	}
}
