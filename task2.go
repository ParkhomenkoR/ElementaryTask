package main

import (
	"fmt"
	"os"
)

type envelop struct {
	width  float64
	height float64
}

func main() {
	for true {

		var env1 = envelop{}
		fmt.Println("Введите длину конверта1 ")
		fmt.Fscan(os.Stdin, &env1.height)
		fmt.Println("Введите ширину конверта1 ")
		fmt.Fscan(os.Stdin, &env1.width)
		var env1HeightInt int = int(env1.height)
		var env1WidthInt int = int(env1.width)

		var env2 = envelop{}
		fmt.Println("Введите длину конверта2 ")
		fmt.Fscan(os.Stdin, &env2.height)
		fmt.Println("Введите ширину конверта2 ")
		fmt.Fscan(os.Stdin, &env2.width)
		var env2HeightInt int = int(env2.height)
		var env2WidthInt int = int(env2.width)

		if env1HeightInt > 0 && env1WidthInt > 0 && env2HeightInt > 0 && env2WidthInt > 0 {
			calculate(env1, env2)
			break
		}
		fmt.Println("{status: ‘failed’, reason: ‘Вводите числа больше 0‘}")
	}
}

func calculate(env1 envelop, env2 envelop) {
	if (env1.height < env2.height && env1.height < env2.width) ||
		(env1.width < env2.width && env1.width < env2.height) {
		fmt.Println("Конверт 2")
	} else if (env1.height > env2.height && env1.height > env2.width) ||
		(env1.width > env2.width && env1.width > env2.height) {
		fmt.Println("Конверт 1")
	} else {
		fmt.Println("0")

	}
}
